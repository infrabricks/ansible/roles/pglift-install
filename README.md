# PGlift install
Ansible role to install Pglift.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name             | Variable description        | Type    | Default | Required
---                       | ---                         | ---     | ---     | ---
pgsql_local_repokeys_path | Local path of repos pubkeys | string  | Yes     | Yes
pgsql_dalibo_repo_url     | Remote Dalibo repo URL      | string  | Yes     | Yes
pgsql_dalibo_repo_pubkey  | Dalibo repo pubkey name     | string  | Yes     | Yes
pgsql_dalibo_repo_keyring | Dalibo repo keyring name    | string  | Yes     | Yes
pgsql_dalibo_packages     | Packages list to install    | string  | Yes     | Yes
pgsql_dba_user            | DBA user name               | string  | Yes     | Yes
pgsql_dba_group           | DBA user group              | string  | Yes     | Yes
pgsql_dba_homedir         | DBA user homedir            | string  | Yes     | Yes
pgsql_dba_user_shell      | DBA user shell              | string  | Yes     | Yes
pgsql_pgbackrest_enabled  | Enable pgBackRest           | boolean | Yes     | Yes
pgsql_postgres_path       | pgsql path                  | string  | Yes     | Yes
pgsql_etc_path            | pglift etc path             | string  | Yes     | Yes
pgsql_run_path            | pglift etc path             | string  | Yes     | Yes
pgsql_log_path            | pglift log path             | string  | Yes     | Yes
pgsql_backup_path         | pglift backup path          | string  | Yes     | Yes

## Dependencies

* [PGlift Ansible collection](https://galaxy.ansible.com/dalibo/pglift)

## Playbook Example

```
---
- name: Install PGlift
  hosts: all
  become: yes
  vars_files:
    - vars/main.yml
    - vars/secret.yml
  roles:
    - pglift_install
```

## License

GPL v3

## Links

* [PostgreSQL](https://www.postgresql.org/)
* [PGlift](https://pglift.readthedocs.io/en/latest/)
* [pgBackRest](https://pgbackrest.org/)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
